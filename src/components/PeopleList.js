import React, { useState, useEffect } from "react";
import { isEmpty } from "underscore";
import { useParams, Link } from "react-router-dom";
import styled from "styled-components";
import axios from "axios";

export const PeopleList = () => {
  let { page } = useParams();
  if (!page) {
    page = 1;
  }
  const [people, setPeople] = useState([]);
  const [pageCount, setPageCount] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setErrorMsg("");

      try {
        const result = await axios(
          `https://swapi.dev/api/people/?page=${page}`
        );
        setPageCount(Math.ceil(result.data.count / 10));
        const peopleArr = [];
        for (let i = 0; i < result.data.results.length; i++) {
          let item = result.data.results[i];
          const id = item.url.match(/[0-9]+/)[0];
          let speciesName = "Unknown species";
          if (!isEmpty(item.species)) {
            const speciesResult = await axios(item.species[0]);

            speciesName = speciesResult.data.name;
          }
          peopleArr.push({ name: item.name, speciesName: speciesName, id: id });
        }

        setPeople(peopleArr);
      } catch (error) {
        setErrorMsg(error.message);
      }
      setIsLoading(false);
    };
    fetchData();
  }, [page]);

  if (errorMsg) {
    return <div>Error: {errorMsg}</div>;
  }

  const peopleElements =
    people &&
    people.map((item, key) => {
      return (
        <Person key={key}>
          <div>
            <div className={"name"}>{item.name}</div>
            <br />
            <div className={"speciesName"}>{item.speciesName}</div>
          </div>
          <PersonDetailButton>
            <Link to={`/detail/${item.id}`}>{">"}</Link>
          </PersonDetailButton>
        </Person>
      );
    });

  const navElements = [];

  for (let i = 1; i <= pageCount; i++) {
    navElements.push(
      <Link key={`navLink${i}`} to={`/${i}`}>
        Page {i}
      </Link>
    );
  }

  return (
    <>
      {isLoading ? <div>Loading ...</div> : <div>{peopleElements}</div>}
      <NavElements>{navElements}</NavElements>
    </>
  );
};

const Person = styled.div`
  display: flex;
  padding: 25px 50px;
  border-bottom: 1px solid lightgray;

  .name {
    color: black;
    font-size: 30px;
  }

  .speciesName {
    color: grey;
    font-size: 20px;
  }
`;

const PersonDetailButton = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;

  a {
    background-color: rgb(237 32 109);
    border: none;
    border-radius: 20em;
    color: white;
    padding: 8px 20px;
    text-align: center;
    text-decoration: none;
    font-size: 12px;
    margin-left: auto;
  }
`;

const NavElements = styled.div`
  display: flex;
  justify-content: center;
  font-size: 12px;
  font-weight: bold;
  padding: 15px 50px;

  a {
    margin-inline: 20px;
    color: grey;
    text-decoration: none;
  }
`;
