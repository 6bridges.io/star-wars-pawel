import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import styled from "styled-components";

export const MoviesList = () => {
  let { id } = useParams();
  const history = useHistory();

  const [person, setPerson] = useState({});
  const [films, setFilms] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setErrorMsg("");

      try {
        const result = await axios(`https://swapi.dev/api/people/${id}/`);
        setPerson(result.data);
        let filmsArr = [];

        for (let i = 0; i < result.data.films.length; i++) {
          const filmResult = await axios(result.data.films[i]);

          filmsArr.push({
            title: filmResult.data.title,
            releaseDate: filmResult.data.release_date,
            director: filmResult.data.director,
          });
        }
        setFilms(filmsArr);
      } catch (error) {
        setErrorMsg(error.message);
      }
      setIsLoading(false);
    };
    fetchData();
  }, [id]);

  if (errorMsg) {
    return <div>Error: {errorMsg}</div>;
  }

  const personName = <Person>{person.name} movies</Person>;

  const filmElements = films.map((item, key) => (
    <Film key={key}>
      <span className={"title"}>
        {" "}
        {item.title} [{item.releaseDate}]
      </span>
      <br />
      <span className={"director"}>Director: {item.director}</span>
      <br />
    </Film>
  ));

  return (
    <>
      {isLoading ? (
        <div>Loading ...</div>
      ) : (
        <div>
          <TitleSection>
            <Button>
              <button onClick={() => history.goBack()}>{"<"}</button>
            </Button>
            {personName}
          </TitleSection>
          {filmElements}
        </div>
      )}
    </>
  );
};

const TitleSection = styled.div`
  padding-inline: 10px;
  display: flex;
  border-bottom: 1px solid lightgray;
`;

const Person = styled.div`
  font-size: 35px;
  padding: 15px 50px;
`;

const Film = styled.div`
  padding: 15px 50px;
  border-bottom: 1px solid lightgray;
  padding: 15px 50px;
  .title {
    color: black;
    font-size: 25px;
  }
  .director {
    color: gray;
    font-size: 20px;
  }
`;

const Button = styled.div`
  align-self: center;
  button {
    cursor: pointer;
    background-color: rgb(237 32 109);
    border: none;
    border-radius: 20em;
    color: white;
    padding: 8px 20px;
    text-align: center;
    text-decoration: none;
    font-size: 12px;
  }
`;
