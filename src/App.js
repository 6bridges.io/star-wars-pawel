import React from "react";
import { Switch, Route } from "react-router-dom";
import { PeopleList, MoviesList } from "./components";
import styled from "styled-components";

function App() {
  return (
    <Wrapper>
      <Switch>
        <Route path="/detail/:id">
          <MoviesList />
        </Route>
        <Route path="/:page">
          <PeopleList />
        </Route>
        <Route path="/">
          <PeopleList />
        </Route>
      </Switch>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  margin: 100px;
  border: 1px solid black;
`;

export default App;
